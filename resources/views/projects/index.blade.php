<!-- /resources/views/projects/index.blade.php -->

 @extends('app')

@section('content')
{{ ddd($projects) }}
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Projects</div>

                <div class="panel-body">
                    @if ( !$projects->count() )
                        You have no projects
                    @else
                        <ul>
                            @foreach( $projects as $project )
                                <li>
                                    {!! Form::open(array('class' => 'form-inline', 'method' => 'DELETE', 'route' => array('projects.destroy', $project->slug))) !!}
                                        <a href="{{ route('projects.show', $project->slug) }}">{{ $project->name }}</a>
                                        (
                                            {!! link_to_route('projects.edit', 'Edit', array($project->slug), array('class' => 'btn btn-info')) !!},
                                            {!! Form::submit('Delete', array('class' => 'btn btn-danger')) !!}
                                        )
                                    {!! Form::close() !!}
                                </li>
                            @endforeach
                        </ul>
                    @endif
                 
                    <p>
                        {!! link_to_route('projects.create', 'Create Project') !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
