<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('contact', 'ContactController@index');

Route::get('hello/{name?}', 'ContactController@hello');

// Provide controller methods with object instead of ID

Route::model('projects', 'Project');
Route::bind('projects', function($value, $route) {
	return App\Project::whereSlug($value)->first();
});
Route::resource('projects', 'ProjectsController'); //

Route::model('tasks', 'Task');
Route::bind('tasks', function($value, $route) {
	return App\Task::whereSlug($value)->first();
});
Route::resource('projects.tasks', 'TasksController');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

